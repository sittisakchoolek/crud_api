import 'package:flutter/material.dart';

import 'package:crud_with_api/data_model/product_data_model.dart';
import 'package:crud_with_api/pages/add_product.dart';
import 'package:crud_with_api/pages/product_detail.dart';
import 'package:http/http.dart' as http;

class ProductList extends StatefulWidget {
  const ProductList({super.key});

  @override
  State<ProductList> createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  late Future<List<ProductDataModel>> products;

  @override
  void initState() {
    super.initState();
    products = getProductList();
  }

  Future<List<ProductDataModel>> getProductList() async {
    String url = "http://10.0.2.2/android_api/list_products.php";
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return productDataModelFromJson(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load products');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Product List'),
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        centerTitle: true,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20.0),
            child: IconButton(
              icon: const Icon(Icons.add),
              tooltip: 'Add Product',
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const AddProduct()),
                );
              },
            ),
          ),
        ],
      ),
      body: Center(
        child: FutureBuilder<List<ProductDataModel>>(
          future: products,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            // By default, show a loading spinner.
            if (!snapshot.hasData) {
              return const CircularProgressIndicator(
                backgroundColor: Colors.tealAccent,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.amberAccent),
                strokeWidth: 5,
              );
            }
            // Render product lists
            return ListView.builder(
              padding: const EdgeInsets.symmetric(vertical: 20),
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                var data = snapshot.data[index];
                return Card(
                  child: ListTile(
                    title: Text(
                      data.name,
                      style: const TextStyle(fontSize: 20),
                    ),
                    subtitle: Text(
                      data.description,
                      style: const TextStyle(fontSize: 15),
                    ),
                    trailing: Text(
                      data.price,
                      style: const TextStyle(fontSize: 20),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ProductDetail(product: data)),
                      );
                    },
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }
}
