import 'package:crud_with_api/pages/edit_product.dart';
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:crud_with_api/data_model/product_data_model.dart';

class ProductDetail extends StatefulWidget {
  const ProductDetail({super.key, required this.product});

  final ProductDataModel product;

  @override
  State<ProductDetail> createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {

  void deleteProduct(context) async {
    await http.post(
      Uri.parse("http://10.0.2.2/android_api/delete_product.php"),
      body: {
        'pid': widget.product.pid.toString(),
      },
    );
    // Navigator.pop(context);
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }
  void confirmDelete(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: const Text('Are you sure you want to delete this?'),
          actions: <Widget>[
            ElevatedButton.icon(
              label: const Text('No'),
              icon: const Icon(Icons.cancel, color: Colors.redAccent),
              style: ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20, color: Colors.white)),
              onPressed: () => Navigator.of(context).pop(),
            ),
            ElevatedButton.icon(
              label: const Text('Yes'),
              icon: const Icon(Icons.check_circle),
              style: ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20, color: Colors.white)),
              onPressed: () => deleteProduct(context),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Product Details'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.delete),
            onPressed: () => confirmDelete(context),
          ),
        ],
      ),
      body: Container(
        height: double.infinity,
        margin: const EdgeInsets.all(18),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.product.name,
              style: const TextStyle(fontSize: 25),
            ),
            const Divider(thickness: 2),
            Container(
              padding: const EdgeInsets.all(15),
              child: Text(
                widget.product.description,
                style: const TextStyle(fontSize: 16),
              ),
            ),
            const Divider(thickness: 2),
            const SizedBox(height: 15),
            Container(
              padding: const EdgeInsets.all(12),
              color: Colors.amberAccent,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  const Text('Price: ', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
                  Text('${widget.product.price}\$', style: const TextStyle(color: Colors.black)),
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.edit),
        onPressed: () => Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => EditProduct(product: widget.product),
          ),
        ),
      ),
    );
  }
}
